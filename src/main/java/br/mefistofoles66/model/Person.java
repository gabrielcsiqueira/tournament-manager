package br.mefistofoles66.model;

import java.util.Date;

public class Person {

	private String name;
	private Date dataNasc;

	public Person(String name, Date dataNasc) {
		this.dataNasc = dataNasc;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDataNasc() {
		return dataNasc;
	}

	public void setDataNasc(Date dataNasc) {
		this.dataNasc = dataNasc;
	}
}
